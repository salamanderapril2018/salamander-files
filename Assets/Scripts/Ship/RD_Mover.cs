﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Robert Dawes-Hegan
 * bullet firing
 * 03/14/2018 to 03/14/2018
 */

public class RD_Mover : MonoBehaviour{
    public float speed;

    // Use this for initialization
    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.up * speed;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
