﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AG_PlayerShooting : MonoBehaviour
{

    /*        

 //        Developer Name: Alan Grubb

 //         Contribution: Implemented the weapon shoot function for the Space Ship.

 //                Feature Enables the Space Ship to fire the lazer.

 //                Start & End dates: 04/10/18 6pm CST 04/16/18 11am CST

    //                References: Q. (2014, November 15). Unity Tutorial: 2D Space Shooter - Part 5 - Shooting and AI. 
                                  Retrieved April 17, 2018, from https://youtu.be/1mw1ufZq1N4

                                  PNGS Sprites:
                                  K. (n.d.). Kenney • Space Shooter Redux. Retrieved April 17, 2018, 
                                  from http://kenney.nl/assets/space-shooter-redux

                                  U. (n.d.). How to correctly implement shooting power-up in Space Shooter game(C#)?? 
                                  Retrieved April 17, 2018, from https://forum.unity.com/threads/how-to-correctly-implement-shooting-power-up-in-space-shooter-game-c.380400/

                                  U. (n.d.). Tags. Retrieved April 19, 2018, 
                                  from https://unity3d.com/learn/tutorials/topics/interface-essentials/tags

                                  Technologies, U. (n.d.). Vector3. Retrieved April 19, 2018, 
                                  from https://docs.unity3d.com/ScriptReference/Vector3.html

 //                        Links: https://youtu.be/1mw1ufZq1N4
                                  https://forum.unity.com/threads/how-to-correctly-implement-shooting-power-up-in-space-shooter-game-c.380400/
                                  https://unity3d.com/learn/tutorials/topics/interface-essentials/tags
                                  https://docs.unity3d.com/ScriptReference/Vector3.html

 //*/


    public GameObject bulletPrefab;

    public float fireDelay = 0.25f;
    float cooldownTimer = 0;


    // Update is called once per frame
    void Update()
    {
        cooldownTimer -= Time.deltaTime;

        if (Input.GetButton("Fire1") && cooldownTimer <= 0 )
        {  // SHOOT LASER!!
            Debug.Log("Pew!");
            cooldownTimer = fireDelay;

            Instantiate(bulletPrefab, transform.position, transform.rotation);
       
        }
    }
}