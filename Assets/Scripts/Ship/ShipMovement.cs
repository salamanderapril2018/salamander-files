﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

    /*
     
       Developer Name: Jessie Campbell
       Contribution: Entire Code 
        Feature: Ship movement
        Start & End Date: Saturday 04/14/18 - Saturday 04/14/18
        Reference: quill18creates.(2014, Nov 14). Unity Tutorial: 2D Space Shooter - Part 2 - Moving Around!.Youtube.[video file]
                 Link: https://www.youtube.com/watch?v=f0GYxSmw328&t=1028s 
        
    */


    public float velocity = 0.1f;
    public float speed = 0.01f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        pos.y += Input.GetAxis("Vertical") * velocity;
        pos.x += Input.GetAxis("Horizontal") * velocity;
        

        transform.position = pos;

    }
}
