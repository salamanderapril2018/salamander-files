﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    /*Robert Micheal Dawes-Hegan
     * The Ship movement plus the ship fire scripts in one
     * 03/10/2018 through 03/11/2018
     * thanks to 3d Voxel cube sampler for providing reference scripts 
     */

public class RD_PlayerMovement : MonoBehaviour
{
    public GameObject Player;
    public float speed;
    public float bulletSpeed = 20.0f;
    public GameObject shot;         // bullet prefab
    public Transform shotSpawn;     // the turret (bullet spawn location)
    public GameObject laser;        // laser prefab

    public float fireRate = 0.5f;
    private float nextFire = 0.0f;

    
    public bool Laser;


    // Use this for initialization
    void Start()
    {

    }

    void FixedUpdate()
    {
        // keyboard
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");


        if (moveHorizontal != 0 || moveVertical != 0)
        {
            Player.GetComponent<Rigidbody>().velocity = new Vector3(moveHorizontal, moveVertical, 0.0f ) * speed;

        }
    }
    void Update()
    {
        if ((Input.GetButton("Fire1") || Input.GetKeyDown(KeyCode.Space)) && Time.time > nextFire)
        {
            if (Laser == true)
            {
                nextFire = Time.time + fireRate;
                Instantiate(laser, shotSpawn.position, shotSpawn.rotation);
                //GetComponent<AudioSource>().Play();  
            }
            else
            {
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
                //GetComponent<AudioSource>().Play();   
            }

        }
       
    }
}
