﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSSpeedUpgrade : MonoBehaviour
{

    /*        

//        Developer Name: Thomas Sanders
//        Contact Info: Email: Sandersthomas1801@gmail.com
//         Contribution: 

//                Feature: UI/Speed/HighScore/Lives

//                Start & End dates: 2/26/18

//                References:

//                        Links:

//*/
    float timerLength = 10.0f;//The length of time in seconds.
    float timerTimePassed = 0.0f;//The variable that will store the time passed while the timer is going.
   public bool runTimer = false;//Used to start/stop the timer.
    public float SpeedBoost = 2;
    
    
  
    


    private void OnTriggerEnter(Collider other) 
    {
        if (other.tag == "SpeedUpgrade")
        {
            runTimer = true;
            gameObject.GetComponent<RD_PlayerMovement>().speed = gameObject.GetComponent<RD_PlayerMovement>().speed + SpeedBoost;
           
        }
    }
    void Update()
    {
        if (runTimer)
        {
            timerTimePassed += Time.deltaTime;
            if (timerTimePassed >= timerLength)
            {
                timerTimePassed = 0f;
                runTimer = false;
                gameObject.GetComponent<RD_PlayerMovement>().speed = gameObject.GetComponent<RD_PlayerMovement>().speed - SpeedBoost;
            }
        }

    }
}