﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Robert Dawes-Hegan
 * The script to change my bool to true for lasers
 * Feature Lasers
 * 3/15/2018 to 03/15/2018
 */

public class RD_Trigger : MonoBehaviour {

    RD_PlayerMovement script;

    void OnTriggerEnter(Collider entity)
    {
        if (entity.tag == "Player")
        {
            script = entity.GetComponentInParent<RD_PlayerMovement>();
            script.Laser = true;
            GameObject.Destroy(this.gameObject);
        }
    }
}
