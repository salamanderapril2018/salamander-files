﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket_Shot : MonoBehaviour
{

    /*
    Name: David Kelly
    Contribution: Adding Projectile when upgrade grabbed.
    Feature: Rocket Upgrade
    Start/End: March 11 / March 16
    */

    public GameObject Self;
    public int Speed = 0;

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.right * Speed;
    }

    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.gameObject.tag == "Enemy")
        {
            Destroy(Self);
        }
        if (Col.gameObject.tag == "Wall")
        {
            Destroy(Self);
        }
        if (Col.gameObject.tag == "Untagged")
        {
            Destroy(Self);
        }
    }
}
