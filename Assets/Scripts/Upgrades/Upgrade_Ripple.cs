﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade_Ripple : MonoBehaviour
{
    /*
    Name: David Kelly
    Contribution: changes basic shot to ripple shot.
    Feature: Ripple Upgrade
    Start/End: March 11 / March 16
    */

    public Transform RippleShotSpawn;
    public GameObject RippleShot;
    public bool HasRipple;


    void Start()
    {
        HasRipple = false;
    }

    void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.gameObject.tag == "Player")
        {
            HasRipple = true;
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (HasRipple == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Instantiate(RippleShot, RippleShotSpawn.position, RippleShotSpawn.rotation);
            }
        }

    }
}
