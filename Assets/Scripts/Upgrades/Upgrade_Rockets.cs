﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade_Rockets : MonoBehaviour
{
    /*
    Name: David Kelly
    Contribution: Adding Projectile when upgrade grabbed.
    Feature: Rocket Upgrade
    Start/End: March 11 / March 16
    */

    public Transform TopRocketSpawn;
    public GameObject TopShot;

    public Transform BotRocketSpawn;
    public GameObject BotShot;

    public bool HasRocket;


    void Start()
    {
        HasRocket = false;
    }

    void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.gameObject.tag == "Player")
        {
            HasRocket = true;
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (HasRocket == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Instantiate(TopShot, TopRocketSpawn.position, TopRocketSpawn.rotation);
                Instantiate(BotShot, BotRocketSpawn.position, BotRocketSpawn.rotation);
            }
        }

    }

}
