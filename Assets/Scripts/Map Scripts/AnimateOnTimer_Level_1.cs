﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateOnTimer_Level_1 : MonoBehaviour
{
    /*
    Name: David Kelly
    Contribution: Timer to start animations
    Feature: Level 1 blockout and enviromental dangers
    Start/End: March 11 / March 16
    */

    public bool Activated;
    public bool Deactivated;
    public int ActivationTime = 0;
    public int DeactivationTime = 0;
    public float StartAxisX = 0;
    public float StartAxisY = 0;
    public float EndAxisX = 0;
    public float EndAxisY = 0;

    public int Speed = 0;
    public Rigidbody2D Wall;

    private int TimePassed;

    void Start ()
    {
        StartCoroutine("Time");
        Activated = false;
        Deactivated = true;
    }
	
	void Update ()
    {
		if (TimePassed == ActivationTime)
        {
            Activated = true;
            Deactivated = false;
        }
        if (TimePassed == DeactivationTime)
        {
            Activated = false;
            Deactivated = true;
        }
        if (Activated == true)
        {
            Speed = 1;
            Wall.velocity = new Vector2(StartAxisX, StartAxisY) * Speed * Speed;
        }
        if (Deactivated == true)
        {
            Speed = 0;
            Wall.velocity = new Vector2(EndAxisX, EndAxisY) * Speed * Speed;
        }



	}

    IEnumerator Time()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            TimePassed++;
            print(TimePassed);
        }
    }

}
