﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapController_Level_1 : MonoBehaviour
{
    /*
    Name: David Kelly
    Contribution: Level 1 Control: Animations, Level Transition.
    Feature: Level 1 Block out and Enviromental Dangers
    Start/End: March 11 / March 16
    */

    public bool IsDead;
    public float HP = 0;
    private int TimePassed;

    public GameObject BreakableWalls;

    void Start()
    {
        IsDead = false;
        StartCoroutine("Time");
    }

    /*
    Enemys will become Awake when LevelTime reaches TimeToActivate
    When Awake their speed becomes 1 and they move left by that speed
    */
    void Update()
    {
        if (IsDead == true)
        {
            Destroy(gameObject);
        }
    }

    /* 
    when a object with tag Shot hits the enemy
    the enemy will lose 1 hp and when hp reaches 0
    the enemy will be destroyed
    */

    void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.gameObject.tag == "Shot")
        {
            HP--;
        }
        if (HP == 0)
        {
            IsDead = true;
        }
        if (Col.gameObject.tag == "Player")
        {
            /*PlayerLives--;
            IsDead = true
            if (playersLives == 0)
            {
                RestartGame();
            }
            */
        }
    }

    // if Players lives run out it will restart the scene.
    void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    IEnumerator Time()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            TimePassed++;
            
        }
    }
}
