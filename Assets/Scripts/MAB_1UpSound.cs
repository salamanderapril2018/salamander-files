﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAB_1UpSound : MonoBehaviour {

	//Developer Name: Marc Abril Bueno
	//        Contact Info: Email: marcabrilbueno@gmail.com
	//         Contribution: Started script from scratch. Downloaded audio clip from the internet to add flavor to the pickup.

	//                Feature: 1Up Capsule

	//                Start & End dates: 04/18/18-04/20/18
	//				  References: Lurony Youtube Tutorial, "Unity 5 - How to Trigger Audio Only Once".
	// Audio retrieved from: http://freesound.org on 04/18/18.

	public AudioClip SoundToPlay;
	public float Volume;
	AudioSource audio;
	public bool played = false;

	void Start () {

		audio = GetComponent<AudioSource> (); //gets the actual file from the gameobject
	}

	void OnTriggerEnter2D (Collider2D col){

		if (!played && col.tag == "Player") { //if clause to determine whether the audio was played or not; could probably be re-coded into regular playaudio, since the pickup is destroyed on collision anyway. 
			//Also added a check for Player tag, so that enemies would not trigger sounds.
			audio.PlayOneShot (SoundToPlay, Volume);
			played = true;
		}
	}
}
