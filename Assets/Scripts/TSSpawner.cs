﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TSSpawner : MonoBehaviour {
    /*        

  //        Developer Name: Thomas Sanders
  //        Contact Info: Email: Sandersthomas1801@gmail.com
  //         Contribution: 

  //                Feature: UI/Speed/HighScore/Lives

  //                Start & End dates: 2/26/18

  //                References:

  //                        Links:

  //*/
    //Array of objects to spawn

    [SerializeField]
    GameObject[] Debrie;
    [SerializeField]
    float TimeMin = 1.0f;
    [SerializeField]
    float TimeMaxed = 4.0f;
    private float TimeLimit = 0;
    private float Timer = 0;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        UpdateTimer();
    }

    void UpdateTimer()
    {
        Timer += Time.deltaTime;
        if (Timer >= TimeLimit)
        {
            TimeLimit = Random.Range(TimeMin, TimeMaxed);
            Timer = 0;
            DoSpawn();

        }
    }

    void DoSpawn()
    {
        int randindex = Random.Range(0, Debrie.Length);
        Instantiate(Debrie[randindex], transform.position, transform.rotation);
    }
}

