﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    List<string> names = new List<string>() { "NORMAL", "RED_GREEN", "BLUE_YELLOW" };

    public AudioMixer audioMixer;
    public Dropdown resolutionDropdown;
    public Dropdown dropdown;
    public Text selectedName;
    public GameObject objectToDisable;
    public static bool disable = false;
    public GameObject Background;
    public GameObject BackgroundREDGREEN;
    public GameObject BackgroundBLUEYELLOW;

    public void Dropdown_IndexChanged(int index)
    {
        selectedName.text = names[index];
        if (index == 0)
        {
            Background.SetActive(true);
            BackgroundREDGREEN.SetActive(false);
            BackgroundBLUEYELLOW.SetActive(false);
        }

        else if (index == 1)
        {
            Background.SetActive(false);
            BackgroundREDGREEN.SetActive(true);
            BackgroundBLUEYELLOW.SetActive(false);
        }

            if (index == 2)
            {
                Background.SetActive(false);
                BackgroundREDGREEN.SetActive(false);
                BackgroundBLUEYELLOW.SetActive(true);
            }
        }
    

        
    

    void Start()
    {
        PopulateList();
    }

    void PopulateList()
    {
        dropdown.AddOptions(names);
    }

    Resolution[] resolutions;

    void Update()
    {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();


    }

    public void SetVolume(float volume)
    {
        Debug.Log(volume);
        audioMixer.SetFloat("volume", volume);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

 
    
}
