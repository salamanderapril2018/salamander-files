﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TSScoreCount : MonoBehaviour {
    /*        

//        Developer Name: Thomas Sanders 
//        Contact Info: Email: Sandersthomas1801@gmail.com

//         Contribution: 

//                Feature: UI/Speed/HighScore/Lives

//                Start & End dates: 2/26/18

//                References:

//                        Links:

//*/
    public Text ScoreCountText; // The way of accessing the Canvas 1Pscore Text in the inspecter
    public Text ScoreCountText2;// The way of accessing the Canvas 2PScore Text in the inspecter
    public int Score; // Score Number
	// Use this for initialization
	void Start () {
        Score = 0;
        setscorecounttext();
		
	}

    void setscorecounttext() // Sets correct number for the Canvas Lives Text
    {
        ScoreCountText.text = "" + Score.ToString();
        ScoreCountText2.text = "" + Score.ToString();
    }
	
	// Update is called once per frame
	void Update () {
        // This Will not be the final setting!!!!! Needs to be changed to whenever player shoots an enemy Score = Score + (Whatever amount that enemy was worth) Contact Me if confused. 
        Score++; // Score keep adding up Each Frame
        setscorecounttext();
	}
}
