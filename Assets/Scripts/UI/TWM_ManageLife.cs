﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TWM_ManageLife : MonoBehaviour {


	//
	//Developer Name: Travis Washburn-Moore
	//Contribution: 1-Up Script
	//Feature: Level 3 1-Up (Salamander}
	//Start and End Dates: Feb 28 2018 - March 16 2018
	//References: gamesplusjames Helped to figure out how to add 1 Life upon pickup
	//Links: https://www.youtube.com/watch?v=zr7ys5lFakA
	//

	public int startingLivesTWM;
	private int lifeCountTWM;

	private Text twmText;

	// Use this for initialization
	void Start () {
		twmText = GetComponent<Text>();

		lifeCountTWM = startingLivesTWM;

		
	}
	
	// Update is called once per frame
	void Update () {

		twmText.text = " " + lifeCountTWM;
		
	}

	public void	ExtraLife()
	{
		lifeCountTWM++;
	}
		
}
