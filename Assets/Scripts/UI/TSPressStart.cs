﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSPressStart : MonoBehaviour {
    /*        

//        Developer Name: Thomas Sanders
//        Contact Info: Email: Sandersthomas1801@gmail.com
//         Contribution: 

//                Feature: UI/Speed/HighScore/Lives

//                Start & End dates: 2/26/18

//                References:

//                        Links:

//*/
    int blinktimer;
    int blinklimit;
    public GameObject Text;

	// Use this for initialization
	void Start () {
        blinktimer = 0;
        // 

        // Blinklimit's number is the amount of frames.
        blinklimit = 100;
		
	}
	
	// Update is called once per frame
	void Update () {
        if (blinktimer < blinklimit)
        {
            blinktimer++;
        }

        else
        {
            blinktimer = 0;
            if(
            Text.activeInHierarchy == false)
            {
                Text.SetActive(true);

            }
            else
            {
                Text.SetActive(false);
            }
        }
            


        
		
	}
}
