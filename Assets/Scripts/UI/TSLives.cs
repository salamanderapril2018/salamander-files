﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TSLives : MonoBehaviour {
    /*        

//        Developer Name: Thomas Sanders
//        Contact Info: Email: Sandersthomas1801@gmail.com
//         Contribution: 

//                Feature: UI/Speed/HighScore/Lives

//                Start & End dates: 2/26/18

	//        Developer Name: Marc Abril Bueno
	//        Contact Info: Email: marcabrilbueno@gmail.com
	//         Contribution: Edited Update. Changed numLives variable to public static int. These changes are so that I can access the Canvas through the MAB_1Up script.

	//                Feature: 1Up Capsule

	//                Start & End dates: 04/18/18-04/20/18

//*/
    public Text LivesCountText; // The way of accessing the Canvas Lives Text in the inspecter
    public static int numLives; // Number Of lives
    public int DeathCount; // How much to take away from numLives if hit
	// Use this for initialization
	void Start () {
        numLives = 2;
        setLivesCountText();
        DeathCount = 1;
	}

    void setLivesCountText() // Sets correct number for the Canvas Lives Text
    {
        LivesCountText.text = "" + numLives.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Add DeathAnimation IF Statement here!!
        //Example: If hit by enivrormental danger or enemy then numLives = numLives - DeathCount Else if num lives is ==(Equal to 0 then pull up pull up death screen(If it was made made!!))
    }

    
    // Update is called once per frame
    void Update () {

		setLivesCountText ();
	}
}
