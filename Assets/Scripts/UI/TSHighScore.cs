﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class TSHighScore : MonoBehaviour {
    /*        

 //        Developer Name: Thomas Sanders
 //        Contact Info: Email: Sandersthomas1801@gmail.com
 //         Contribution: 

 //                Feature: UI/Speed/HighScore/Lives

 //                Start & End dates: 2/26/18

 //                References:

 //                        Links:

 //*/
    public int ScoreCounter;
    public Text HighScoreText;
    public int HighScore;
	// Use this for initialization
	void Start () {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            HighScore = PlayerPrefs.GetInt("HighScore");
        }
       
        
       

        ScoreCounter = gameObject.GetComponent<TSScoreCount>().Score;
    }
	
	// Update is called once per frame
	void Update () {



        ScoreCounter = gameObject.GetComponent<TSScoreCount>().Score;

        if 
            (ScoreCounter > HighScore)
        {
            HighScore = ScoreCounter;
            PlayerPrefs.SetInt("HighScore", HighScore);
            PlayerPrefs.Save();

        }
        HighScoreText.text = "" + Mathf.Round(HighScore);



    }
}
