﻿using System.Collections;
using UnityEngine;


public class TWM_ExtraLife : MonoBehaviour {



	//
	//Developer Name: Travis Washburn-Moore
	//Contribution: 1-Up Script
	//Feature: Level 3 1-Up (Salamander}
	//Start and End Dates: Feb 28 2018 - March 16 2018
	//References: gamesplusjames Helped to figure out how to add 1 Life upon pickup
	//Links:  https://www.youtube.com/watch?v=zr7ys5lFakA
	//


	private TWM_ManageLife twmLife;

	// Use this for initialization
	void Start () {
		twmLife = FindObjectOfType<TWM_ManageLife> ();
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Player") 
		{
			twmLife.ExtraLife ();
			Destroy (gameObject);
		}
		
	}
}
