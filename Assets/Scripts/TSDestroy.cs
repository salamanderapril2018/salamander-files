﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSDestroy : MonoBehaviour {

    public GameObject MySelf;

     private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(MySelf);
        }
    }
}
