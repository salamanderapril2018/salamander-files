﻿using UnityEngine;
using System.Collections;

public class JM_ValisFortBehavior : MonoBehaviour {

	public GameObject ship;
	public GameObject ammo;
	public GameObject core1;
	public GameObject core2;
	public GameObject core3;

	// Use this for initialization
	void Start () {

	}

	void OnTriggerEnter (Collider col){
		if (col.gameObject == ship){
			GameObject Appear = GameObject.Find("ValisWall");
			(Appear).GetComponent<MeshRenderer> ().enabled = true;
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}

/*END NOTES*/

//        Developer Name: Jennifer Miller

//         Contribution: Behavior script for mook enemies.

//                March 09 - March 16.
//				  script was intended to also change color of fort from wall to green.
//				  script was also intended to destroy fort on all three cores being destroyed.

//                References: previous scripts authored by self.
