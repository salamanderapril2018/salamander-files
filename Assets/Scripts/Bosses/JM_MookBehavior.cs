﻿using UnityEngine;
using System.Collections;

public class JM_MookBehavior : MonoBehaviour {

	public GameObject self;
	public GameObject ship;
	public GameObject ammo;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (self.GetComponent<MeshRenderer> ().enabled == true) {
			transform.Translate (0,0,-3f);
		}

	}

	void OnCollisionEnter (Collision col) {
		if (col.gameObject == ship) {
			Destroy(col.gameObject);
			Destroy(this.gameObject);
		}
		if (col.gameObject == ammo) {
			Destroy(this.gameObject);
		}
	} //end CollisionEnter
}

/*END NOTES*/

//        Developer Name: Jennifer Miller

//         Contribution: Behavior script for mook enemies.

//                March 09 - March 16.
//				  enemies do not fire bullets at player with this script.
//				  script is incomplete as to the unique behaviors of each enemy.

//                References: previous scripts authored by self.
