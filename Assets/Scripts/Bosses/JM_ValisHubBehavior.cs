﻿using UnityEngine;
using System.Collections;

public class JM_ValisHubBehavior : MonoBehaviour {

	public GameObject core;
	private int hp;
	public GameObject ammo;
	public string blep; //this is the fort's shield ammunition

	// Use this for initialization
	void Start () {
	
		hp = 10;

	}

	void OnCollisionEnter (Collision col) {

		if (col.gameObject == ammo){
			hp--;
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (hp > 0 & hp < 10) {
			GameObject Appear = GameObject.FindGameObjectWithTag (blep);
			(Appear).GetComponent<MeshRenderer> ().enabled = true;
		}

		if (hp <= 0) {
			Destroy (this.gameObject);
		}

	}
}

/*END NOTES*/

//        Developer Name: Jennifer Miller

//         Contribution: Behavior script for mook enemies.

//                March 09 - March 16.
//				  script is incomplete: requires spawning of shield ammo.

//                References: previous scripts authored by self.
