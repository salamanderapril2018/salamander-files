﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class Enemies_Level_1 : MonoBehaviour
{
    /*
    Name: David Kelly
    Contribution: Player Tracking and Death
    Feature: Level 1 Enemies
    Start/End: March 11 / March 16
    */

    public bool Awake;
    public bool IsDead;
    public float HP = 0;
    public int TimeToActivate = 0;
    public int Speed = 0;

    private int LevelTime;
    public Rigidbody2D enemy;




    void Start ()
    {
        Awake = false;
        IsDead = false;
        StartCoroutine("ActivateTime");
        LevelTime = 0;
        Speed = 0;
    }

    /*
    Enemys will become Awake when LevelTime reaches TimeToActivate
    When Awake their speed becomes 1 and they move left by that speed
    */
    void Update ()
    {
        if (TimeToActivate == LevelTime)
        {
            Awake = true;
        }
        if (Awake == true)
        {
            Speed = 1;
            enemy.velocity = new Vector2 (1, 0)* -1 * Speed;
        }
        if (IsDead == true)
        {
            Destroy(gameObject);
        }
    }

    /* 
    when a object with tag Shot hits the enemy
    the enemy will lose 1 hp and when hp reaches 0
    the enemy will be destroyed
    */
    void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.gameObject.tag == "Shot")
        {
            HP--;
        }
        if (HP == 0)
        {
            IsDead = true;
        }
        if (Col.gameObject.tag == "Player")
        {
            /*PlayerLives--;
            IsDead = true;
            if (playersLives == 0)
            {
                RestartGame();
            }
            */
            
        }
    }

    // if Players lives run out it will restart the scene.
    void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // A World Timer
    IEnumerator ActivateTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            LevelTime++;
            
        }
    }
}
