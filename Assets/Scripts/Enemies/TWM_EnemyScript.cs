﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TWM_EnemyScript : MonoBehaviour {
	//
	//Developer Name: Travis Washburn-Moore
	//Contribution: Enemy Spawner and Movement
	//Feature: Level 3 Enemy (Salamander}
	//Start and End Dates: Feb 28 2018 - March 16 2018
	//References: Pixelement Games Helped to figure out how to destroy game object upon leaving the area and how to move from one direction to another
	//Links:  https://www.youtube.com/watch?v=k3GN3FuyqnQ&t=548s
	//

	float twmSpeed;


	// Use this for initialization
	void Start () {
		twmSpeed = 3f;
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 position = transform.position;

		position = new Vector2 (position.x, position.y - twmSpeed * Time.deltaTime);

		transform.position = position;

		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));

		if (transform.position.y < min.y) 
		{
			Destroy (gameObject);
		}
	}
}
