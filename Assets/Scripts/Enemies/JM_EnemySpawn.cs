﻿using UnityEngine;
using System.Collections;

public class JM_EnemySpawn : MonoBehaviour {

	public string area;
	public GameObject ship;

	// Use this for initialization
	void OnTriggerEnter (Collider col) {
		if (col.gameObject == ship) {
			print ("I have entered a new screen.");
			GameObject Appear = GameObject.FindGameObjectWithTag (area);
			(Appear).GetComponent<MeshRenderer> ().enabled = true;
		}
	}

}

/*END NOTES*/

//        Developer Name: Jennifer Miller

//         Contribution: Script to spawn enemies on a trigger collision.

//                March 09 - March 16.

//                References: previous scripts authored by self.

//                        Links: https://answers.unity.com/questions/1041678/how-do-i-make-all-game-objects-with-a-certain-tag.html
