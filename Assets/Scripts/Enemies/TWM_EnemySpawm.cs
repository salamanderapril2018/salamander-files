﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TWM_EnemySpawm : MonoBehaviour {

	//
	//Developer Name: Travis Washburn-Moore
	//Contribution: Enemy Spawner and Movement
	//Feature: Level 3 Enemy (Salamander}
	//Start and End Dates: Feb 28 2018 - March 16 2018
	//References: Pixelement Games Helped to figure out how to spawn enemies coming from a random area on the map and keep spawning in intervals
	//Links:  https://www.youtube.com/watch?v=k3GN3FuyqnQ&t=548s
	//

	public GameObject Enemy;
	float maxSpawnRateISTWM = 3f;

	// Use this for initialization
	void Start () {
		Invoke ("SpawnEnemy", maxSpawnRateISTWM);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void SpawnEnemy()
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));

		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));

		GameObject anEnemy = (GameObject)Instantiate (Enemy);
		anEnemy.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);
	}
}
