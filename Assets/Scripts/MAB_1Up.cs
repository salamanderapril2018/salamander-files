﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAB_1Up : MonoBehaviour {


	//Developer Name: Marc Abril Bueno
	//        Contact Info: Email: marcabrilbueno@gmail.com
	//         Contribution: Created script from scratch. Simple, run of the mill counter. Accesses the variable, "numLives" from the main UI script.

	//                Feature: 1Up Capsule

	//                Start & End dates: 04/18/18-04/20/18
	void OnTriggerEnter2D (Collider2D col){

		TSLives.numLives++; //Adds one life to the counter.

		Destroy (gameObject); //Destroys the pickup.
	}
}
	
