﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	//Armando Harris


	public GameObject Camera; 	//The camera used to Scroll. !!! The ship must be a child of the camera. 
	private float Move;			//Tells where in the level the camera has gone
	public int ScrollSpeed;		//Adjust this for how fast to scroll the camera
	private Vector3 CameraTrans; //To store the Camera's position
	private bool LevelEnded;  	//To know when to stop the camera from scrolling
	public GameObject Ship;		//the Ship used in game
	private Vector3 ShipTrans;	//the ship's local position
	public float ShipSpeed;		//set how fast ship navigates
	public Rigidbody2D Bullet;	//the bullet to instantiate. Use prefab here
	public float BulletSpeed;	//how fast bullet travels
	private bool ReadyShot;		//Bool to see if ship can fire
	private float ShotTimer;	//Timer to allow time inbetween shots
	public int ShotSpeed;		//Adjust for how fast ship can shoot
	public float BossStage;		//Adjust for when camera should stop at boss stage
	public Rigidbody2D Barrier;	//Objects for Core's Barriers
	public Rigidbody2D Barrier2;
	public Rigidbody2D Barrier3;
	public Rigidbody2D Barrier4;
	public Rigidbody2D Barrier5;
	public Rigidbody2D Barrier6;
	public Rigidbody2D Barrier7;
	public Rigidbody2D Barrier8;
	public Rigidbody2D Barrier9;
	public GameObject Core;		//Objects for Cores
	public GameObject Core2;
	public GameObject Core3;
	public Rigidbody2D BossBalls;//Objejts core's will shoot. use prefab here
	private bool BossShot;		//bool to see if boss can shoot
	private float BossShotTimer;//timer to allow time inbetween boss' shots
	public float BossShotSpeed;	//adjust this to how fast the cores can shoot
	public float BossBulletSpeed;//adjust this for how fast the boss' shot will travel
	private bool CoreTurn;		//to set which core will shoot
	private bool CoreTurn2;
	private bool CoreTurn3;










	// Use this for initialization

	void Start () 
	{

		LevelEnded = false;
		Move = 0;
		CameraTrans = Camera.transform.position;
		ShipTrans = Ship.transform.localPosition;
		ReadyShot = true;
		ShotTimer = 0;
		ShotSpeed = 25;
		ScrollSpeed = 6;
		ShipSpeed = 7;
		BulletSpeed = 35;
		BossStage = 50;
		BossShot = true;
		CoreTurn = true;
		CoreTurn2 = false;
		CoreTurn3 = true;
		BossShotSpeed = 5;
		BossBulletSpeed = 1;
	}

	// Update is called once per frame
	void Update () 
	{
		//To keep the camera scrolling until LevelEnded = true. The camera will stop scrolling.
		//If want to scroll to the right, change CameraTrans.y to .x and rotate ship's Z to 0
		if (LevelEnded == false) 
		{
			Move = Move + (ScrollSpeed * Time.deltaTime);
			CameraTrans.y = Move;
			Camera.transform.position = CameraTrans;
		
		//To set the Move to stop at or a little above BossStage. Adjust BossStage 
		//to see where the camera should stop.
			if (Move >= BossStage) 
			{
				LevelEnded = true;
			}


		}

		//To adjust how fast the Boss shoots from the cores. Adjust the BossShotSpeed to 
		//increase or decrease the rate of fire.
		BossShotTimer += (BossShotSpeed * Time.deltaTime);
		if (BossShotTimer >= 10)
		{
			BossShot = true;
			BossShotTimer = 0;
		}

		//To constantly call ControlShip to allow movement.
		ControlShip ();

		//To adjust how fast the ship shoots. Adjust the ShotSpeed to increase 
		//or decrease the rate of fire.
		ShotTimer += (ShotSpeed * Time.deltaTime);
		if (ShotTimer >= 10) 
		{
			ReadyShot = true;
			ShotTimer = 0;
		}

		//To constantly call function BossBattle to allow cores to fire when levelEnded = true;
		BossBattle ();
	}

	//Function that allows keyboard controls to control the ship. 
	///!!! The Ship must be a child of the camera to  function properly. 
	/// The .localPosition refers to the child's position within the parent.
	void ControlShip()
	{
		if (Input.GetKey(KeyCode.D) == true) 
		{
			ShipTrans.x = ShipTrans.x + (ShipSpeed * Time.deltaTime);
			Ship.transform.localPosition = ShipTrans;
		}
		if (Input.GetKey(KeyCode.A) == true) 
		{
			ShipTrans.x = ShipTrans.x - (ShipSpeed * Time.deltaTime);
			Ship.transform.localPosition = ShipTrans;
		}
		if (Input.GetKey(KeyCode.S) == true) 
		{
			ShipTrans.y = ShipTrans.y - (ShipSpeed * Time.deltaTime);
			Ship.transform.localPosition = ShipTrans;
		}
		if (Input.GetKey(KeyCode.W) == true) 
		{
			ShipTrans.y = ShipTrans.y + (ShipSpeed * Time.deltaTime);
			Ship.transform.localPosition = ShipTrans;
		}
		if (Input.GetKey(KeyCode.L) == true) 
		{
			Shoot ();
		}

	}

	void Shoot()
	{
		//to set a local variable Shots that will become the instantiated Bullet objects
		Rigidbody2D Shots;

		//To allow ship to fire when ReadyShot = true. 
		if (ReadyShot == true)
		{

		//To make instances of the Bullet at the Ship's position and rotation.
		Shots = Instantiate (Bullet, Ship.transform.position, Ship.transform.rotation);

		//To make the vector of the Shots and speed of the shot by adjusting BulletSpeed.
		Shots.velocity = Ship.transform.TransformDirection(Vector3.right * BulletSpeed);
		
		//To allow time inbetween shots. 
		ReadyShot = false;
		ShotTimer = 0;

		}


	}

	void BossBattle()
	{
		//Similar to void Shoot(). Balls are instances of BossBalls
		Rigidbody2D Balls;

		//Checks if reached boss stage, Boss ready to shoot, and if it is Core's turn to shoot.
		//Core's turn to shoot and then it loops to Core 2's turn
		if (LevelEnded == true && BossShot == true && CoreTurn == true) 
		{
			
			Balls = Instantiate (BossBalls, Core.transform.position, Ship.transform.rotation);
			Balls.velocity = (Ship.transform.position - Core.transform.position) * BossBulletSpeed;
			BossShot = false;
			BossShotTimer = 0;
			CoreTurn = false;
			CoreTurn2 = true;
		}
		//Core 2's turn and loops to Core 3's turn
		if (LevelEnded == true && BossShot == true && CoreTurn2 == true) 
		{

			Balls = Instantiate (BossBalls, Core2.transform.position, Ship.transform.rotation);
			Balls.velocity = (Ship.transform.position - Core2.transform.position) * BossBulletSpeed;
			BossShot = false;
			BossShotTimer = 0;
			CoreTurn2 = false;
			CoreTurn3 = true;
		}
		//Core 3's turn, and loops to Core 1's turn
		if (LevelEnded == true && BossShot == true && CoreTurn3 == true) 
		{

			Balls = Instantiate (BossBalls, Core3.transform.position, Ship.transform.rotation);
			Balls.velocity = (Ship.transform.position - Core3.transform.position) * BossBulletSpeed;
			BossShot = false;
			BossShotTimer = 0;
			CoreTurn3 = false;
			CoreTurn = true;
		}
	}
	 
}
