﻿/*
    Name: Anaiza Rodriguez
    Contribution: Entire Code
    Feature: Stage 1 - Brain Golem (Boss) Arms and Boss Death
    Start/End: 4/17/2018 - 
*/
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BrainGolem_Stage1 : MonoBehaviour {

    public bool BossIsAwake;
    public bool BossIsDead;

    public float HEALTH = 0;    //amount of health the boss has

    private int SPEED = 0;
    public int TIMEActivated = 0;

    private int TIMELevel;

    public Rigidbody2D BOSS;

    // Use this for initialization
    void Start () {

        BossIsAwake = false;
        BossIsDead = false;

        StartCoroutine("ActivateTime");

        TIMELevel = 0;
        SPEED = 0;

    }
	
	// Update is called once per frame
	void Update () {

        if (TIMEActivated == TIMELevel)
        {
            BossIsAwake = true;
        }

        if (BossIsAwake == true)
        {
            SPEED = 0;
            BOSS.velocity = new Vector2(1, 0) * -1 * SPEED;
        }

        if (BossIsDead == true)
        {
            Destroy(gameObject);
            SceneManager.LoadScene("TS.Level2", LoadSceneMode.Additive);
        }
    }

    void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.gameObject.tag == "Shot")
        {
            HEALTH--;
        }

        if (HEALTH == 0)
        {
            BossIsDead = true;
        }

        if (Col.gameObject.tag == "Player")
        {

        }
    }
    
    void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // A World Timer
    IEnumerator ActivateTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            TIMELevel++;
        }
    }
}
