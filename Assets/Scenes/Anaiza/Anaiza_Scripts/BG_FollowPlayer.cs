﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BG_FollowPlayer : MonoBehaviour {

    public float SPEED = 0.3f;
    public GameObject player;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        Vector3 localPosition = player.transform.position - transform.position;
        localPosition = localPosition.normalized; // The normalized direction in LOCAL space
        transform.Translate(localPosition.x * Time.deltaTime * SPEED, localPosition.y * Time.deltaTime * SPEED, localPosition.z * Time.deltaTime * SPEED);
    }
}
