﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG_PlayerTrigger : MonoBehaviour {

    public BrainGolem_Stage1 _BrainGolem;

    void OnTriggerEnter2D(Collider2D other)
    {

        Debug.Log("Object entered the trigger");
    }

    void OnTriggerStay2D(Collider2D other)
    {

        Debug.Log("Object is within the trigger");
    }

    void OnTriggerExit2D(Collider2D other)
    {

        Debug.Log("Object exited the trigger");
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
